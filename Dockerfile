FROM alpine

RUN apk update && apk add g++ make cmake valgrind

WORKDIR /searching_exercise

COPY . .

RUN cmake -S . -Bbuild

WORKDIR /searching_exercise/build

RUN make

RUN touch result.csv

#CMD ["./run_tests"]
CMD ./timing_template >> result.csv 

#To build "docker build -t t01 ."
#To run "docker run -it --rm --name t01 t01"
#To kill (force to stop) "docker kill t01"