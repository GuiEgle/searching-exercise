/*
 */

#include <iostream>
#include "../include/searching.h"

namespace sa {

    /*!
     * Performs a **linear search** for `value` in `[first;last)` and returns a pointer to the location of the first occurrence of `value` in the range `[first,last]`, or `last` if no such element is found.
     * \param first Pointer to the begining of the data range.
     * \param last Pointer just past the last element of the data range.
     * \param value The value we are looking for.
     */
    value_type * lsearch( value_type * first, value_type * last, value_type value )
    {
        // TODO: Insert here your solution for the linear search problem.
        unsigned step{0};
        while((first+step)<last){
            if (*(first+step)==value)
                return (first+step);
            ++step;
        }

        return last; // stub, not valid.
    }

    /*!
     * Performs a **binary search** for `value` in `[first;last)` and returns a pointer to the location of `value` in the range `[first,last]`, or `last` if no such element is found.
     * \note The range **must** be sorted.
     * \param first Pointer to the begining of the data range.
     * \param last Pointer just past the last element of the data range.
     * \param value The value we are looking for.
     */
    value_type * bsearch( value_type * first, value_type * last, value_type value )
    {
        // TODO: Insert here your solution for the binary search problem.
        size_t length;
        value_type * pivot;
        value_type * notFound{last};
        do{
            length = size_t(last-first);
            pivot = first+(int)(length/2);
            if (*pivot>value){
                last=pivot;
            }
            else if (*pivot<value){
                first=pivot+1;

            }
            else{
                return pivot;
            }
        }while (first<last);
        
        return notFound; // stub, not valid.
    }

    value_type * rbsearch( value_type * first, value_type * last, value_type value,  value_type * notFound)
    {
        // TODO: Insert here your solution for the binary search problem.
        size_t length{size_t(last-first)};
        value_type * pivot{first+(int)(length/2)};
        if (first>=last) return notFound; 
        if (*pivot>value){
            return rbsearch(first, pivot, value, notFound);
        }
        else if (*pivot<value){
            first=pivot+1;
            return rbsearch(pivot+1, last, value, notFound);
        }
        else{
            return pivot;
        }
         // stub, not valid.
    }

    /*!
     * Returns a pointer to the first element in the range `[first, last)` that is _not less_  than (i.e. greater or equal to) `value`, or `last` if no such element is found.
     * \note The range **must** be sorted.
     * \param first Pointer to the begining of the data range.
     * \param last Pointer just past the last element of the data range.
     * \param value The value we are looking for.
     */
    value_type * lbound( value_type * first, value_type * last, value_type value )
    {
        // TODO: Insert here your solution for the lower bound problem.
        size_t length;
        value_type * pivot;
        value_type * notFound{last};
        if (*first>=value) return first; 
        do{
            length = size_t(last-first);
            pivot = first+(int)(length/2);
            if (*pivot>=value){
                    if (*(pivot-1)<value)
                        return pivot;
                    else
                        last=pivot;
            }
            else if (*pivot<value){
                first=pivot+1;
            }
        }while (first<last);
        
        return notFound; // stub, not valid.
    }

    /*!
     * Returns a pointer to the first element in the range `[first, last)` that is _greater_  than `value`, or `last` if no such element is found.
     * \note The range **must** be sorted.
     * \param first Pointer to the begining of the data range.
     * \param last Pointer just past the last element of the data range.
     * \param value The value we are looking for.
     */
    value_type * ubound( value_type * first, value_type * last, value_type value )
    {
        // TODO: Insert here your solution for the upper bound problem.
        size_t length;
        value_type * pivot;
        value_type * notFound{last};
        if (*first>value) return first;
        do{
            length = size_t(last-first);
            pivot = first+(int)(length/2);
            if (*pivot>value){
                last=pivot;
            }
            else if (*pivot<=value){
                if (*(pivot+1)>value)
                    return (pivot+1);
                else
                    first=pivot+1;
            }
        }while (first<last);

        return notFound; // stub, not valid. // stub, not valid.
    }
}

